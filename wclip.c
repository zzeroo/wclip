/*
 * This is free and unencumbered software released into the public domain.

 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.

 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
*/

#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/uio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>

static struct wl_display* wl_display;
static struct wl_seat* wl_seat;
static struct wl_data_device_manager* wl_data_device_manager;
static struct wl_data_device* wl_data_device;
static struct wl_compositor* wl_compositor;
static struct wl_shell* wl_shell;
static struct wl_shm* wl_shm;
static struct wl_shm_pool* wl_shm_pool;
static struct wl_buffer* wl_buffer;
static struct wl_surface* wl_surface;
static char tmp_file[] = "/tmp/wclip.XXXXXX";

static void cleanup_exit(int status)
{
	if(wl_surface)
		wl_surface_destroy(wl_surface);
	if(wl_buffer)
		wl_buffer_destroy(wl_buffer);
	if(wl_shm_pool)
		wl_shm_pool_destroy(wl_shm_pool);
	if(unlink(tmp_file))
		fprintf(stderr, "Failed to delete temp file(%s): %s\n", tmp_file, strerror(errno));
	if(wl_display)
		wl_display_disconnect(wl_display);
	exit(status);
}

static int32_t temp_fd(void)
{
	int32_t fd = mkstemp(tmp_file);
	if(fd < 0)
	{
		fprintf(stderr, "Error while creating temporary file at \"%s\": %s\n", strerror(errno), tmp_file);
		cleanup_exit(1);
	}
	return fd;
}

static int manual_splice(int source, int dest)
{
	/* 1 MB */
	static const size_t BLOCK_SIZE = 1 << 20;
	void *buf = malloc(BLOCK_SIZE);
	do
	{
		int ret = read(source, buf, BLOCK_SIZE);
		if(ret <= 0)
			return ret;
		void *cur = buf, *end = buf + ret;
		do
		{
			ret = write(dest, cur, end - cur);
			/* If write keeps returning 0 for some reason
			 * this can cause an infinite loop */
			if(ret < 0)
				return ret;
			cur += ret;
		}
		while(cur != end);
	}
	while(1);
}

static int splice_all(int source, int dest)
{
	/* 64KB */
	static const size_t BLOCK_SIZE = 1 << 16;
	do
	{
		ssize_t ret = splice(source, NULL, dest, NULL, BLOCK_SIZE, 0);
		if(ret <= 0)
			return ret;
	}
	while(1);
}

static int32_t copy_read_data(void)
{
	int32_t fd = temp_fd();
	if(manual_splice(0, fd) < 0)
	{
		fprintf(stderr, "Error while reading from stdin: %s\n", strerror(errno));
		cleanup_exit(2);
	}
	return fd;
}

static void copy_wl_data_source_target(void *data, struct wl_data_source *wl_data_source, const char *mime_type)
{
	/* This is ignored because the program currently only
	 * supports one data mime type at a time */
}

static void copy_wl_data_source_send(void* data, struct wl_data_source* wl_data_source, const char* mime_type, int32_t fd)
{
	int32_t source_fd = *(int32_t *)data;
	if(lseek(source_fd, 0, SEEK_SET))
	{
		fprintf(stderr, "Error while resetting temp file offset: %s\n", strerror(errno));
		cleanup_exit(3);
	}
	if(manual_splice(source_fd, fd) < 0)
	{
		fprintf(stderr, "Error writing data to receiver file descriptor: %s\n", strerror(errno));
		cleanup_exit(4);
	}
	if(close(fd))
	{
		fprintf(stderr, "Cannot close receiver file descriptor: %s\n", strerror(errno));
		cleanup_exit(5);
	}
}

static void copy_wl_data_source_cancelled(void* data, struct wl_data_source* wl_data_source)
{
	/* Clipboard has been replaced or otherwise should exit */
	cleanup_exit(0);
}

static void copy_wl_data_source_dnd_drop_performed(void *data, struct wl_data_source *wl_data_source)
{
	fprintf(stderr, "wl_data_source.dnd_drop_performed\n");
}

static void copy_wl_data_source_dnd_finished(void *data, struct wl_data_source *wl_data_source)
{
	fprintf(stderr, "wl_data_source.dnd_finished\n");
}

static void copy_wl_data_source_action(void *data, struct wl_data_source *wl_data_source, uint32_t dnd_action)
{
	fprintf(stderr, "wl_data_source.action\n");
}

static void copy(const char** mime, int32_t *fd)
{
	struct wl_data_source* wl_data_source = wl_data_device_manager_create_data_source(wl_data_device_manager);
	struct wl_data_source_listener listener = 
	{
		.target = &copy_wl_data_source_target,
		.send = &copy_wl_data_source_send,
		.cancelled = &copy_wl_data_source_cancelled,
		.dnd_drop_performed = &copy_wl_data_source_dnd_drop_performed,
		.dnd_finished = &copy_wl_data_source_dnd_finished,
		.action = &copy_wl_data_source_action,
	};
	wl_data_source_add_listener(wl_data_source, &listener, fd);
	while(*mime)
		wl_data_source_offer(wl_data_source, *mime++);
	wl_data_device_set_selection(wl_data_device, wl_data_source, 0);
	while(wl_display_dispatch(wl_display) > 0);
	fprintf(stderr, "wl_dispatch event loop returned error\n");
	cleanup_exit(8);
}

static void paste_wl_shell_surface_ping(void* data, struct wl_shell_surface* wl_shell_surface, uint32_t serial)
{
	wl_shell_surface_pong(wl_shell_surface, serial);
}

static void paste_wl_shell_surface_configure(void* data, struct wl_shell_surface* wl_shell_surface, uint32_t edges, int32_t width, int32_t height)
{
}

static const char* mime_match;

static void paste_wl_data_offer_offer(void* data, struct wl_data_offer* wl_data_offer, const char* mime_type)
{
	/* In case MIME types are offered in no particular order */
	const char **mime = (const char **)data;
	while(*mime)
	{
		if(mime_match == *mime)
			break;
		if(strcmp(*mime, mime_type) == 0)
		{
			mime_match = *mime;
			break;
		}
		mime++;
	}
}

static void paste_wl_data_offer_source_actions(void *data, struct wl_data_offer *wl_data_offer, uint32_t source_actions)
{
	fprintf(stderr, "wl_data_offer.source_actions\n");
}

static void paste_wl_data_offer_action(void *data, struct wl_data_offer *wl_data_offer, uint32_t dnd_action)
{
	fprintf(stderr, "wl_data_offer.action");
}

static void paste_wl_data_device_data_offer(void* data, struct wl_data_device* wl_data_device, struct wl_data_offer* id)
{
	/* We just have to hope that the user didn't manage
	 * to drop something on a 1x1 window */
	static const struct wl_data_offer_listener wl_data_offer_listener =
	{
		.offer = &paste_wl_data_offer_offer,
		.source_actions = &paste_wl_data_offer_source_actions,
		.action = &paste_wl_data_offer_action,
	};
	wl_data_offer_add_listener(id, &wl_data_offer_listener, data);
}

static void paste_wl_data_device_enter(void *data, struct wl_data_device *wl_data_device, uint32_t serial, struct wl_surface *surface, wl_fixed_t x, wl_fixed_t y, struct wl_data_offer *id)
{
	fprintf(stderr, "wl_data_device.enter\n");
}

static void paste_wl_data_device_leave(void *data, struct wl_data_device *wl_data_device)
{
	fprintf(stderr, "wl_data_device.leave\n");
}

static void paste_wl_data_device_motion(void *data, struct wl_data_device *wl_data_device, uint32_t time, wl_fixed_t x, wl_fixed_t y)
{
	fprintf(stderr, "wl_data_device.motion\n");
}

static void paste_wl_data_device_drop(void *data, struct wl_data_device *wl_data_device)
{
	fprintf(stderr, "wl_data_device.drop\n");
}

static void paste_wl_data_device_selection(void* data, struct wl_data_device* wl_data_device, struct wl_data_offer* id)
{
	if(!mime_match)
	{
		fprintf(stderr, "No data with the specified MIME type(s)\n");
		cleanup_exit(1);
	}
	int pipefd[2];
	pipe(pipefd);
	wl_data_offer_receive(id, mime_match, pipefd[1]);
	close(pipefd[1]);
	wl_display_roundtrip(wl_display);
	if(splice_all(pipefd[0], 1))
	{
		fprintf(stderr, "Error while transferring data: %s\n", strerror(errno));
		cleanup_exit(2);
	}
	close(pipefd[0]);
	cleanup_exit(0);
}

static void paste(const char** mime)
{
	static const int32_t w_width = 1;
	static const int32_t w_height = 1;
	static const int32_t shm_size = w_width * w_height * 4;
	int32_t fd = temp_fd();
	int ret = posix_fallocate(fd, 0, shm_size);
	if(ret)
	{
		/* posix_fallocate() does not set errno */
		fprintf(stderr, "Falied to allocate space in temp file: %s\n", strerror(ret));
		cleanup_exit(6);
	}
	wl_shm_pool = wl_shm_create_pool(wl_shm, fd, shm_size);
	wl_buffer = wl_shm_pool_create_buffer(wl_shm_pool, 0, w_width, w_height, w_width * 4, WL_SHM_FORMAT_ARGB8888);
	wl_surface = wl_compositor_create_surface(wl_compositor);
	wl_surface_attach(wl_surface, wl_buffer, 0, 0);
	struct wl_shell_surface* wl_shell_surface = wl_shell_get_shell_surface(wl_shell, wl_surface);
	struct wl_shell_surface_listener wl_shell_surface_listener =
	{
		.ping = &paste_wl_shell_surface_ping,
		.configure = &paste_wl_shell_surface_configure,
	};
	wl_shell_surface_add_listener(wl_shell_surface, &wl_shell_surface_listener, NULL);
	struct wl_data_device_listener wl_data_device_listener =
	{
		.data_offer = &paste_wl_data_device_data_offer,
		.enter = &paste_wl_data_device_enter,
		.leave = &paste_wl_data_device_leave,
		.motion = &paste_wl_data_device_motion,
		.drop = &paste_wl_data_device_drop,
		.selection = &paste_wl_data_device_selection,
	};
	wl_data_device_add_listener(wl_data_device, &wl_data_device_listener, mime);
	wl_shell_surface_set_toplevel(wl_shell_surface);
	wl_shell_surface_set_title(wl_shell_surface, "wclip");
	wl_surface_commit(wl_surface);
	while(wl_display_dispatch(wl_display) > 0);
	fprintf(stderr, "Did not receive keyboard focus\n");
	cleanup_exit(2);
}

static void registry_global(void* data, struct wl_registry* registry, uint32_t name, const char* interface, uint32_t version)
{
	if(strcmp(interface, wl_data_device_manager_interface.name) == 0)
		wl_data_device_manager = wl_registry_bind(registry, name, &wl_data_device_manager_interface, version);
	else if(strcmp(interface, wl_seat_interface.name) == 0)
		wl_seat = wl_registry_bind(registry, name, &wl_seat_interface, version);
	else if(strcmp(interface, wl_compositor_interface.name) == 0)
		wl_compositor = wl_registry_bind(registry, name, &wl_compositor_interface, version);
	else if(strcmp(interface, wl_shell_interface.name) == 0)
		wl_shell = wl_registry_bind(registry, name, &wl_shell_interface, version);
	else if(strcmp(interface, wl_shm_interface.name) == 0)
		wl_shm = wl_registry_bind(registry, name, &wl_shm_interface, version);
}

static void init_wl(void)
{
	wl_display = wl_display_connect(NULL);
	if(!wl_display)
	{
		fprintf(stderr, "Unable to open display(is the $DISPLAY environment variable set?)\n");
		cleanup_exit(7);
	}
	struct wl_registry* wl_registry = wl_display_get_registry(wl_display);
	if(!wl_registry)
	{
		fprintf(stderr, "Unable to get Wayland registry\n");
		wl_display_disconnect(wl_display);
		cleanup_exit(8);
	}
	struct wl_registry_listener wl_registry_listener =
	{
		.global = &registry_global,
		.global_remove = NULL,
	};
	wl_registry_add_listener(wl_registry, &wl_registry_listener, NULL);
	wl_display_roundtrip(wl_display);
	wl_registry_destroy(wl_registry);
	wl_data_device = wl_data_device_manager_get_data_device(wl_data_device_manager, wl_seat);
}

static void usage(const char* executable)
{
	printf("%s i <mime_types>\n"
	"\tCopy stdin into clipboard(input) with the MIME types"
	"\n\tgiven as a space separated list\n"
	"%s o <mime_types>\n"
	"\tPaste clipboard to stdout(output), trying each MIME type in"
	"\n\tthe order given until a match is found"
	"\n\n\tIf no MIME type is provided, the program will default to text\n", executable, executable);
}

int main(int argc, const char** argv)
{
	if(!(argc >= 2 && (argv[1][0] == 'i' || argv[1][0] == 'o')))
	{
		usage(argv[0]);
		return 10;
	}
	int32_t fd;
	if(argv[1][0] == 'i')
	{
		fd = copy_read_data();
		close(0);
		/* stdout is not used beyond this point;
		 * errors are reported to stderr */
		close(1);
		daemon(0, 1);
	}
	init_wl();
	static const char* default_mime[] =
	{
		"text/plain",
		"TEXT",
		"STRING",
		NULL,
	};
	/* argv is also null terminated */
	const char **mime = argc == 2 ? default_mime : (argv + 2);
	if(argv[1][0] == 'i')
		copy(mime, &fd);
	else
		paste(mime);
}
